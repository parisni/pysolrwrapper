from enum import Enum


class PerimeterEnum(Enum):
    CONCEPT = "CONCEPT"
    RELATIONSHIP = "RELATIONSHIP"


class SusanaLoader:
    def __init__(self, conn, m_project_type_id, domain_id, m_language_id):
        """
        Loader Susana Class

        :param conn:
        :param m_project_type_id:
        :param domain_id:
        :param m_language_id:
        """
        self.conn = conn
        self.m_project_type_id = m_project_type_id
        self.domain_id = domain_id
        self.m_language_id = m_language_id

    def create_tmp(self) -> None:
        """
        CREATE UNLOGGED TABLE concept_tmp(
            concept_name text,
            concept_code text NOT NULL,
            m_language_id text,
            m_project_type_id text,
            domain_id text,
            vocabulary_id text,
            m_statistic_type_id text,
            m_value_as_number double precision,
            concept_class_id text);

        CREATE UNLOGGED TABLE concept_relationship_tmp(
            concept_code text NOT NULL,
            vocabulary_id_target text NOT NULL,
            concept_code_target text NOT NULL,
            vocabulary_id text NULL,
            relationship_id text NULL);

        CREATE UNLOGGED TABLE vocabulary_tmp(
            vocabulary_id text NOT NULL,
            vocabulary_name text NOT NULL,
            vocabulary_reference text NULL,
            vocabulary_version text NULL);

        CREATE UNLOGGED TABLE concept_synonym_tmp(
            concept_code text NOT NULL,
            concept_synonym_name text NOT NULL,
            language_concept_id bigint NOT NULL);
        """

    def purge_omop(self) -> None:
        f"""
        WITH del AS (
            DELETE FROM concept
            WHERE lower(vocabulary_id) IN (
                    SELECT
                        lower(vocabulary_id)
                    FROM
                        concept_tmp)
                    AND domain_id IN ('{self.domain_id}', 'Metadata')
                    AND m_project_id IN (
                        SELECT
                            m_project_id
                        FROM
                            mapper_project
                        WHERE
                            lower(m_project_type_id) = lower('{self.m_project_type_id}'))
                    RETURNING
                        concept_id
        ),
        del_statistic AS (
            DELETE FROM mapper_statistic
            WHERE m_concept_id IN (
                    SELECT
                        concept_id
                    FROM
                        del)
        ),
        del_synonym AS (
            DELETE FROM concept_synonym
            WHERE concept_id IN (
                    SELECT
                        concept_id
                    FROM
                        del)
        ),
        del_vocabulary AS (
            DELETE FROM vocabulary
            WHERE lower(vocabulary_id) IN (
                    SELECT
                        lower(vocabulary_id)
                    FROM
                        concept_tmp)
                RETURNING
                    vocabulary_concept_id
        ),
        del_voc_concept AS (
            DELETE FROM concept
            WHERE concept_id IN (
                    SELECT
                        vocabulary_concept_id
                    FROM
                        del_vocabulary)
        ),
        delete_rel1 AS (
            DELETE FROM concept_relationship
            WHERE concept_id_1 IN (
                    SELECT
                        concept_id
                    FROM
                        del))
        DELETE FROM concept_relationship
        WHERE concept_id_2 IN (
                SELECT
                    concept_id
                FROM
                    del)
        """

    def refresh_omop(self) -> None:
        """
        WITH allc AS (
            SELECT
                concept_name,
                coalesce(concept_code, 'UNKNOWN') concept_code,
                m_language_id,
                coalesce(m_project_id, 1) AS m_project_id,
                domain_id,
                vocabulary_id,
                now()::date valid_start_date,
                '20990101'::date valid_end_date,
                concept_tmp.concept_class_id
            FROM
                concept_tmp
                LEFT JOIN mapper_project
                ON lower(concept_tmp.m_project_type_id)
                    = lower(mapper_project.m_project_type_id)
        ),
        ins AS (
        INSERT INTO concept (concept_name, concept_code, m_language_id, m_project_id,
        domain_id, vocabulary_id, valid_start_date, valid_end_date, concept_class_id)
            SELECT
                a.concept_name,
                a.concept_code,
                a.m_language_id,
                a.m_project_id,
                a.domain_id,
                a.vocabulary_id,
                a.valid_start_date,
                a.valid_end_date,
                a.concept_class_id
            FROM
                allc a
                LEFT JOIN concept ON a.vocabulary_id = concept.vocabulary_id
                    AND a.concept_code = concept.concept_code
            WHERE
                concept.vocabulary_id IS NULL
        ),
        upt AS (
            UPDATE
                concept
            SET
                concept_name = a.concept_name,
                concept_code = a.concept_code,
                m_language_id = a.m_language_id,
                m_project_id = a.m_project_id,
                domain_id = a.domain_id,
                vocabulary_id = a.vocabulary_id,
                valid_start_date = a.valid_start_date,
                valid_end_date = a.valid_end_date,
                concept_class_id = a.concept_class_id
            FROM
                allc a
            WHERE
                concept.vocabulary_id = a.vocabulary_id
                AND concept.concept_code = a.concept_code)
            -- , del as (
            -- UPDATE concept SET valid_end_date = now()
            -- WHERE
            -- vocabulary_id IN (select vocabulary_id from concept_tmp)
            -- AND (vocabulary_id, concept_code)
            -- NOT IN (select vocabulary_id, concept_code from concept_tmp)
            -- )
            INSERT INTO mapper_job (m_concept_id, m_job_type_id, m_status_id, m_user_id)
            SELECT
                concept.concept_id,
                'MAPPING',
                0,
                0
            FROM
                concept_tmp
                JOIN concept ON (concept.concept_code = concept_tmp.concept_code
                        AND concept_tmp.vocabulary_id = concept.vocabulary_id)
        """

    def refresh_omop_relationship(self) -> None:
        """
        WITH ins_relationship_to AS (
        INSERT INTO concept_relationship (concept_id_1, concept_id_2, relationship_id,
         valid_start_date, valid_end_date, m_user_id)
            SELECT
                c1.concept_id,
                c.concept_id,
                r.relationship_id,
                now(),
                '2099-01-01',
                0
            FROM
                concept_relationship_tmp r
                JOIN concept c ON (lower(c.concept_code) = lower(r.concept_code_target)
                        AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target))
                JOIN concept c1 ON (lower(c1.concept_code) = lower(r.concept_code)
                        AND lower(c1.vocabulary_id) = lower(r.vocabulary_id))
                LEFT JOIN concept_relationship c_r ON (c_r.concept_id_1 = c1.concept_id
                        AND c_r.concept_id_2 = c.concept_id
                        AND lower(c_r.relationship_id) = lower(r.relationship_id))
            WHERE
                c_r.concept_id_1 IS NULL
            RETURNING
                concept_id_1
        ),
        ins_relationship_from AS (
        INSERT INTO concept_relationship (concept_id_1, concept_id_2, relationship_id,
        valid_start_date, valid_end_date, m_user_id)
            SELECT
                c.concept_id,
                c1.concept_id,
                rel.reverse_relationship_id,
                now(),
                '2099-01-01',
                0
            FROM
                concept_relationship_tmp r
                JOIN relationship rel ON (rel.relationship_id = r.relationship_id)
                JOIN concept c ON (lower(c.concept_code) = lower(r.concept_code_target)
                        AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target))
                JOIN concept c1 ON (lower(c1.concept_code) = lower(r.concept_code)
                        AND lower(c1.vocabulary_id) = lower(r.vocabulary_id))
                LEFT JOIN concept_relationship c_r ON (c_r.concept_id_1 = c.concept_id
                        AND c_r.concept_id_2 = c1.concept_id
                        AND c_r.relationship_id = rel.relationship_id)
            WHERE
                c_r.concept_id_1 IS NULL
            RETURNING
                concept_id_1)
        INSERT INTO mapper_job (m_concept_id, m_job_type_id, m_status_id, m_user_id)
        SELECT
            concept_id_1,
            'MAPPING',
            0,
            0
        FROM
            ins_relationship_to
        UNION ALL
        SELECT
            concept_id_1,
            'MAPPING',
            0,
            0
        FROM
            ins_relationship_from
        """

    def load_omop(self) -> None:
        f"""
        WITH ins AS (
        INSERT INTO concept (concept_name, concept_code, m_language_id, m_project_id,
        domain_id, vocabulary_id, valid_start_date, valid_end_date, concept_class_id)
            SELECT
                concept_name,
                coalesce(concept_code, 'UNKNOWN'),
                m_language_id,
                coalesce(m_project_id, 1) AS m_project_id,
                domain_id,
                vocabulary_id,
                now()::date,
                '20990101',
                concept_tmp.concept_class_id
            FROM
                concept_tmp
                LEFT JOIN mapper_project ON lower(concept_tmp.m_project_type_id)
                                          = lower(mapper_project.m_project_type_id)
            RETURNING
                concept_id,
                concept_code
        ),
        voc_cpt AS (
        INSERT INTO concept (concept_name, concept_code, m_language_id, m_project_id,
        domain_id, vocabulary_id, valid_start_date, valid_end_date, concept_class_id)
            SELECT
                vocabulary_name,
                '{self.m_project_type_id}' || ' ' || 'generated',
                '{self.m_language_id}',
                coalesce(m_project_id, 0) AS m_project_id,
                'Metadata',
                '{self.m_project_type_id} - Vocabulary',
                now()::date,
                '20990101',
                'Vocabulary'
            FROM
                vocabulary_tmp
                LEFT JOIN mapper_project ON lower('{self.m_project_type_id}')
                                          = lower(mapper_project.m_project_type_id)
            WHERE
                lower(vocabulary_id) IN (
                    SELECT
                        lower(vocabulary_id)
                    FROM
                        concept_tmp)
                RETURNING
                    concept_id,
                    concept_name,
                    m_project_id
        ),
        ins_vocabulary AS (
        INSERT INTO vocabulary (vocabulary_id, vocabulary_name, vocabulary_reference,
                                vocabulary_version, vocabulary_concept_id, m_project_id)
            SELECT
                vocabulary_id,
                vocabulary_name,
                vocabulary_reference,
                vocabulary_version,
                voc_cpt.concept_id,
                voc_cpt.m_project_id
            FROM
                vocabulary_tmp
                JOIN voc_cpt ON voc_cpt.concept_name = vocabulary_tmp.vocabulary_name
            WHERE
                lower(vocabulary_id) IN (
                    SELECT
                        lower(vocabulary_id)
                    FROM
                        concept_tmp)
        ),
        ins_statistic AS (
        INSERT INTO mapper_statistic (m_concept_id, m_statistic_type_id, m_value_as_number,
                                      m_algo_id, m_user_id, m_valid_start_datetime)
            SELECT
                concept_id,
                m_statistic_type_id,
                m_value_as_number,
                4,
                10,
                now()
            FROM
                ins
                JOIN concept_tmp USING (concept_code)
            WHERE
                m_value_as_number IS NOT NULL
        ),
        ins_relationship_to AS (
        INSERT INTO concept_relationship (concept_id_1, concept_id_2, relationship_id,
                                          valid_start_date, valid_end_date, m_user_id)
            SELECT
                ins.concept_id,
                c.concept_id,
                r.relationship_id,
                now(),
                '2099-01-01',
                0
            FROM
                ins
                JOIN concept_relationship_tmp r ON (r.concept_code = ins.concept_code)
                JOIN concept c ON (lower(c.concept_code) = lower(r.concept_code_target)
                        AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target))
        ),
        ins_relationship_from AS (
        INSERT INTO concept_relationship (concept_id_1, concept_id_2, relationship_id,
                                          valid_start_date, valid_end_date, m_user_id)
            SELECT
                c.concept_id,
                ins.concept_id,
                rel.reverse_relationship_id,
                now(),
                '2099-01-01',
                0
            FROM
                ins
            JOIN concept_relationship_tmp r ON (r.concept_code = ins.concept_code)
            JOIN relationship rel ON (rel.relationship_id = r.relationship_id)
            JOIN concept c ON (lower(c.concept_code) = lower(r.concept_code_target)
                    AND lower(c.vocabulary_id) = lower(r.vocabulary_id_target))
        ),
        ins_job AS (
        INSERT INTO mapper_job (m_concept_id, m_job_type_id, m_status_id, m_user_id)
            SELECT
                concept_id,
                'MAPPING',
                0,
                0
            FROM
                ins)
            INSERT INTO concept_synonym (concept_id, concept_synonym_name,
                                           language_concept_id, m_user_id)
            SELECT
                ins.concept_id,
                concept_synonym_name,
                t.language_concept_id,
                0
            FROM
                concept_synonym_tmp t
            JOIN ins ON (ins.concept_code = t.concept_code)
        """

    def drop_tmp(self) -> None:
        """
        DROP TABLE IF EXISTS concept_tmp;
        DROP TABLE IF EXISTS concept_relationship_tmp;
        DROP TABLE IF EXISTS vocabulary_tmp;
        DROP TABLE IF EXISTS concept_synonym_tmp;
        """

    def load_tmp(self) -> None:
        """
        Bulk-loads the data into the temporary tables


        """
        pass

    def run(self):
        try:
            self.drop_tmp()
            self.create_tmp()
            self.load_tmp()
            self.purge_omop()
            self.load_omop()
        finally:
            self.drop_tmp()

    def refresh(self, perimeter) -> None:
        try:
            self.drop_tmp()
            self.create_tmp()
            self.load_tmp()
            if perimeter == PerimeterEnum.CONCEPT:
                self.refresh_omop()
            if perimeter == PerimeterEnum.RELATIONSHIP:
                self.refresh_omop_relationship()
        finally:
            self.drop_tmp()
