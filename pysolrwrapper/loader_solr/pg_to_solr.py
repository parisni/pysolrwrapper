sep1 = "\t"
sep2 = "|"
sql_transform = f"""
WITH sub_concept AS (
    SELECT
        concept_id,
        susana.format_text(concept_name, '{sep1}', '{sep2}') concept_name,
        domain_id,
        susana.format_text(vocabulary_id, '{sep1}', '{sep2}') vocabulary_id,
        concept_class_id,
        standard_concept,
        susana.format_text(concept_code, '{sep1}', '{sep2}') concept_code,
        m_language_id,
        m_project_id,
        susana.format_text(invalid_reason, '{sep1}', '{sep2}') invalid_reason
    FROM
        susana.concept
),
sub_project AS (
    SELECT
        m_project_id,
        m_project_type_id
    FROM
        susana.mapper_project
),
stats_avg AS (
    SELECT
        m_concept_id AS concept_id,
        m_value_as_number AS m_value_avg
    FROM
        susana.mapper_statistic
    WHERE
        m_statistic_type_id = 'AVG'
        AND m_value_as_number IS NOT NULL
),
stats_freq AS (
    SELECT
        m_concept_id AS concept_id,
        m_value_as_number AS m_frequency_value
    FROM
        susana.mapper_statistic
    WHERE
        m_statistic_type_id = 'FREQ'
        AND m_value_as_number IS NOT NULL
),
sub_synonym AS (
    SELECT
        concept_id,
        susana.format_text(concept_synonym_name, '{sep1}', '{sep2}') concept_synonym_name,
        language_concept_id
    FROM
        susana.concept_synonym
),
sub_concept_relationship AS (
    SELECT
        concept_id_1,
        concept_id_2,
        relationship_id
    FROM
        susana.concept_relationship
    WHERE
        concept_id_1 != concept_id_2
        AND m_modif_end_datetime IS NULL
        AND invalid_reason IS DISTINCT FROM 'D'
),
dfsynen AS (
    SELECT
        concept_id,
        string_agg(concept_synonym_name, '{sep2}') AS concept_synonym_name_en
    FROM
        susana.concept_synonym
        JOIN sub_concept USING (concept_id)
    WHERE
        concept_synonym_name != concept_name
        AND concept_synonym.language_concept_id = 4180186 -- ENGLISH
    GROUP BY
        concept_id
),
dfsynfr AS (
    SELECT
        concept_id,
        string_agg(concept_synonym_name, '{sep2}') AS concept_synonym_name_fr
    FROM
        susana.concept_synonym
        JOIN sub_concept USING (concept_id)
    WHERE
        concept_synonym_name != concept_name
        AND concept_synonym.language_concept_id = 4180190 -- FRENCH
    GROUP BY
        concept_id
),
mappeddf AS (
    SELECT
        cr.concept_id_1 AS concept_id,
        string_agg(cpt3.concept_name, '{sep2}') AS standard_concept_mapped_name,
        string_agg(cpt4.concept_name, '{sep2}') AS non_standard_concept_mapped_name,
        string_agg(cpt5.concept_name, '{sep2}') AS concept_relation_name
FROM
    sub_concept_relationship AS cr
    LEFT JOIN sub_concept AS cpt3 ON (cr.concept_id_2 = cpt3.concept_id
            AND lower(cr.relationship_id) IN ('maps to')
            AND lower(cpt3.standard_concept) IS NOT DISTINCT FROM 's')
    LEFT JOIN sub_concept AS cpt4 ON (cr.concept_id_2 = cpt4.concept_id
            AND lower(cr.relationship_id) IN ('maps to')
            AND lower(cpt4.standard_concept) IS DISTINCT FROM 's')
    LEFT JOIN sub_concept AS cpt5 ON (cr.concept_id_2 = cpt5.concept_id
            AND lower(cr.relationship_id)
            NOT IN ('maps to'))
    GROUP BY
        cr.concept_id_1
),
dfmapen AS (
    SELECT
        cr.concept_id_1 AS concept_id,
        string_agg(cpt.concept_name, '{sep2}') AS concept_mapped_name_en
    FROM
        sub_concept_relationship AS cr
        LEFT JOIN sub_concept AS cpt ON (cr.concept_id_2 = cpt.concept_id
                AND lower(cr.relationship_id) IN ('maps to', 'mapped from'))
    WHERE
        cpt.m_language_id IS NULL
        OR cpt.m_language_id = 'EN'
    GROUP BY
        cr.concept_id_1
),
dfmapfr AS (
    SELECT
        cr.concept_id_1 AS concept_id,
        string_agg(cpt.concept_name, '{sep2}') AS concept_mapped_name_fr
    FROM
        sub_concept_relationship AS cr
        LEFT JOIN sub_concept AS cpt ON (cr.concept_id_2 = cpt.concept_id
                AND lower(cr.relationship_id) IN ('maps to', 'mapped from'))
    WHERE
        cpt.m_language_id = 'FR'
    GROUP BY
        cr.concept_id_1
),
dfhierafr AS (
    SELECT
        cr.concept_id_1 AS concept_id,
        string_agg(cpt.concept_name, '{sep2}') AS concept_hierarchy_name_fr
    FROM
        sub_concept_relationship AS cr
        LEFT JOIN sub_concept AS cpt ON (cr.concept_id_2 = cpt.concept_id
                AND lower(cr.relationship_id) IN ('is a'))
    WHERE
        cpt.m_language_id = 'FR'
    GROUP BY
        cr.concept_id_1
),
dfhieraen AS (
    SELECT
        cr.concept_id_1 AS concept_id,
        string_agg(cpt.concept_name, '{sep2}') AS concept_hierarchy_name_en
    FROM
        sub_concept_relationship AS cr
        LEFT JOIN sub_concept AS cpt ON (cr.concept_id_2 = cpt.concept_id
                AND lower(cr.relationship_id) IN ('is a'))
    WHERE
        cpt.m_language_id = 'EN'
    GROUP BY
        cr.concept_id_1
),
localmapdf AS (
    SELECT
        concept_id_2 AS concept_id,
        count(1) AS local_map_number
    FROM
        sub_concept_relationship
    WHERE
        concept_id_1 >= 2000000000
    GROUP BY
        concept_id_2
)
SELECT
    cast(concept_id AS text) AS id,
    concept_id,
    susana.format_text(COALESCE(concept_name, 'EMPTY'), '{sep1}', '{sep2}') AS concept_name,
    susana.format_text(CASE WHEN m_language_id IS NULL
        OR m_language_id = 'EN' THEN
        COALESCE(concept_name, 'EMPTY')
    ELSE
        NULL
    END, '{sep1}', '{sep2}') AS concept_name_en,
    susana.format_text(CASE WHEN m_language_id = 'FR' THEN
        COALESCE(concept_name, 'EMPTY')
    ELSE
        NULL
    END, '{sep1}', '{sep2}') AS concept_name_fr,
    susana.format_text(CASE WHEN m_language_id = 'FR' THEN
        COALESCE(concept_name, 'EMPTY')
    ELSE
        NULL
    END, '{sep1}', '{sep2}') AS concept_name_fr_en,
    susana.format_project(COALESCE(domain_id, 'EMPTY'), m_project_type_id) AS domain_id,
    susana.format_project(COALESCE(vocabulary_id, 'EMPTY'), m_project_type_id) AS vocabulary_id,
    susana.format_project(COALESCE(concept_class_id, 'EMPTY'), m_project_type_id) AS concept_class_id,
    susana.format_project(COALESCE(standard_concept, 'EMPTY'), m_project_type_id) AS standard_concept,
    susana.format_project(COALESCE(invalid_reason, 'EMPTY'), m_project_type_id) AS invalid_reason,
    COALESCE(concept_code, 'EMPTY') AS concept_code,
    concept_synonym_name_en concept_synonym_name_en,
    concept_synonym_name_fr concept_synonym_name_fr,
    concept_synonym_name_fr AS concept_synonym_name_fr_en,
    concept_mapped_name_en concept_mapped_name_en ,
    concept_mapped_name_fr concept_mapped_name_fr,
    concept_hierarchy_name_en concept_hierarchy_name_en,
    concept_hierarchy_name_fr concept_hierarchy_name_fr,
    concept_mapped_name_fr AS concept_mapped_name_fr_en,
    susana.format_project (COALESCE(m_language_id, 'EN'), m_project_type_id) AS m_language_id,
    m_frequency_value AS frequency,
    susana.format_project (
        CASE WHEN standard_concept_mapped_name IS NOT NULL THEN
            'S'
        WHEN non_standard_concept_mapped_name IS NOT NULL THEN
            'NS'
        WHEN concept_relation_name IS NOT NULL THEN
            'R'
        ELSE
            'EMPTY'
        END, m_project_type_id) AS is_mapped,
    COALESCE(local_map_number, 0) AS local_map_number,
    m_value_avg AS value_avg,
    m_value_avg IS NOT NULL value_is_numeric,
    m_project_type_id AS m_project_type_id
FROM
    sub_concept
    LEFT JOIN mappeddf USING (concept_id)
    LEFT JOIN dfsynen USING (concept_id)
    LEFT JOIN dfsynfr USING (concept_id)
    LEFT JOIN dfmapen USING (concept_id)
    LEFT JOIN dfhieraen USING (concept_id)
    LEFT JOIN dfhierafr USING (concept_id)
    LEFT JOIN dfmapfr USING (concept_id)
    LEFT JOIN localMapDF USING (concept_id)
    LEFT JOIN stats_avg USING (concept_id)
    LEFT JOIN stats_freq USING (concept_id)
    LEFT JOIN sub_project USING (m_project_id)
"""
