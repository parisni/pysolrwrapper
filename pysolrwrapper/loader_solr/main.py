import argparse

from pysolrwrapper.loader_postgres.susana_loader import SusanaLoader

parser = argparse.ArgumentParser(description="Loads Susana Postgres Database")


def main():
    args = parser.parse_args()
    SusanaLoader(conn=None, domain_id=None, m_language_id=None, m_project_type_id=None)


def prepare_database():
    pass


if __name__ == "__main__":
    main()
