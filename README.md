# Install

## Design

```
+-----------------+                         +------------------+
|                 |                         |                  |
|     SOLR        |                         |   POSTGRESQL     |
|                 |                         |      OMOP        |
|                 |                         |    DATABASE      |
|                 |                         |                  |
|                 |                         |                  |
+-----------------+                         |                  |
|                 |                         |                  |
|      PYTHON     <-------------------------+                  |
|   PG-SOLR-SYNC  |                         |                  |
|                 |                         |                  |
|                 |                         |                  |
+-----------------+   +-----------------+   +------------------+
                      |   JAVASCRIPT    |
                      |    OPENUI-5     |
                      |                 |
                      +-----------------+
                      |                 |
                      |     PYTHON      |
                      |     FLASK       |
                      |     BACKEND     |
                      |                 |
                      +-----------------+
```

## Solr

- loading a csv into solr http://localhost:8983/solr/new_core/update?stream.file=/opt/bitnami/solr/example/exampledocs/books.csv&commit=true&stream.contentType=text/csv;charset%3Dutf-8
- activate the remote streaming https://solr.apache.org/guide/8_8/requestdispatcher-in-solrconfig.html
- solr optimizaation: https://stackoverflow.com/a/9452626
- solr csv config https://solr.apache.org/guide/6_6/uploading-data-with-index-handlers.html#UploadingDatawithIndexHandlers-CSVUpdateParameters
- https://solr.apache.org/guide/8_8/uploading-data-with-index-handlers.html#csv-update-parameters
- enableStreamBody (stream.body) allows to pass the data as a POST
- commitWithin, commit, overwritecommit

python
- https://www.devdungeon.com/content/python-use-stringio-capture-stdout-and-stderr
- https://www.psycopg.org/docs/cursor.html#cursor.copy_expert

Gotcha:
- the csv must be accessible from solr
- it can be produced by postgres copy
- a python script with psycopg2 can be run
- a full streaming could happen postgres->solr with copy to stdout -> stream.body
- a scheduling can be run from https://github.com/agronholm/apscheduler

## Pypi

```
pip install tox
tox
```

## Upload to pypi

```bash
# bump pysolrwrapper/__version__.py
flit publish
```


# Usage
```
from pysolrwrapper.core import SolrQuery
from pysolrwrapper.filter import (FilterText, FilterColumnEnum)

    result = SolrQuery("localhost:9983", "omop-concept")\
        .set_type_edismax()\
        .select(["concept_id","concept_name"])\
        .add_filter(FilterText(["disease lethal", "patient"]))\
        .add_filter(FilterColumnEnum("standard_concept", ["c","s"]))\
        .add_filter(FilterColumnEnum("standard_concept", ["*"]))\
        .highlight(["concept_name", "synonym_concept_name"])\
        .facet(["standard_concept", "domain_id"], limit=10, missing=True, matches=['myRegexPattern'])\
        .sort("score", "asc")\
        .sort("concept_id", "asc")\
        .limit(2)\
	.run()
```

# Results

Depending on what you ask for, the result is a dictionnary with this format:

```
{'num_shown': 2,
 'num_found': 7052,
 'docs': [{'concept_id': '42742376',
           'concept_name': 'Molecular pathology procedure, Level 7'},
          {'concept_id': '2107646',
           'concept_name': 'Direct repair of aneurysm, pseudoaneurysm, or excision(partial or total) and graft insertion, with or without patch graft for ruptured aneurysm, abdominal aorta involving iliac vessels(common, hypogastric, external)'}],
    'facet_fields': {'standard_concept': [{'s': 6263},
                                          {'c': 789}],
                     'domain_id': [{'condition': 2870},
                                   {'measurement': 1621},
                                   {'observation': 1481},
                                   {'drug': 659},
                                   {'procedure': 303},
                                   {'meas': 100},
                                   {'value': 100},
                                   {'provider': 13},
                                   {'specialty': 13},
                                   {'device': 3}]
                     }
 }
```
