CREATE TABLE extract AS
SELECT
    distinct
    concept_id
FROM
   concept
WHERE
    concept_id in (
    select concept_id_1
    from concept_relationship
    where relationship_id = 'Maps to'
    and concept_id_1 > 2000000000
    limit 10000
    )
    and vocabulary_id !~ 'APHP'
limit 100;

\copy (select * from concept where concept_id in (select concept_id from extract)) to '/tmp/concept.csv' header csv;
\copy (select * from concept_relationship where concept_id_1 in (select concept_id from extract) and concept_id_2 in (select concept_id from extract)) to '/tmp/concept_relationship.csv' header csv;
\copy (select * from concept_synonym where concept_id in (select concept_id from extract)) to '/tmp/concept_synonym.csv' header csv;
\copy (select * from vocabulary where vocabulary_id in (select vocabulary_id from concept where concept_id in (select concept_id from extract))) to '/tmp/vocabulary.csv' header csv;
\copy (select * from relationship ) to '/tmp/relationship.csv' header csv;
\copy (select * from mapper_user where m_lastname ~* 'parrot|paris' limit 2) to '/tmp/mapper_user.csv' header csv;
\copy (select * from map.mapper_project) to '/tmp/mapper_project.csv' header csv;
\copy (select * from mapper_role where m_user_id in (select m_user_id from mapper_user where m_lastname ~* 'parrot|paris' limit 2)) to '/tmp/mapper_role.csv' header csv;
