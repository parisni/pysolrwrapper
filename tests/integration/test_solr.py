import pysolr


def test_solr_is_up(solr, solr_client):
    core = "susana_core"
    res = solr_client.solr_query(
        f"/{core}/update/csv?stream.body=1%3B%27hey%27%2C%27you%27&commit=true&separator=%3B&f.category.split=true&f.category.separator=%2C&f.category.encapsulator=%27&fieldnames=concept_id,concept_synonym_name_en"
    )
    assert res.status_code == 200
    solr = pysolr.Solr(f"{solr_client.conf}/susana_core", auth=solr_client.auth)
    result = solr.search("*:*")
    print(result)
