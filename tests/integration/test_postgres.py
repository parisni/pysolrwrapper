import pandas as pd
import pysolr
from pandas._testing import assert_frame_equal

from pysolrwrapper.core import SolrQuery
from pysolrwrapper.loader_postgres.postgres_utils import PostgresClient
from pysolrwrapper.loader_solr.pg_to_solr import sql_transform
from pysolrwrapper.utils import resource_path_root
from tests.conftest import SOLR_DATA


def test_postgres(pgcon, tmpdir):
    pg_client = PostgresClient(pgcon)
    print(pg_client.run("create table foo (a int, b text)"))
    print(pg_client.run("insert into foo values (1, 'foo'), (2, 'bar')"))
    sql = "select * from\n foo where a = 1"
    tmpcsv = tmpdir + "/foo.csv"
    pg_client.bulk_export(sql, tmpcsv)

    pdcsv = pd.read_csv(tmpcsv)
    goal = pd.DataFrame({"a": [1], "b": ["foo"]})
    assert_frame_equal(goal, pdcsv)


def test_susana_sample_loaded(susana_sample_con):
    assert (
        len(pd.read_sql("select * from susana.concept", susana_sample_con).index) == 100
    )


def test_susana_sample_loaded(susana_sample_con):
    assert (
        len(pd.read_sql("select * from susana.mapper_user", susana_sample_con).index) == 2
    )


def test_format_project_function(susana_con):
    sql = """\
   select
   format_project('hello - world', 'nothing') as a,
   format_project('world', 'hello') as b;
   """
    goal = pd.DataFrame({"a": ["hello - world"], "b": ["hello - world"]})
    assert_frame_equal(goal, pd.read_sql(sql, susana_con))


def test_format_test_function(susana_con):
    sql = """\
   select
   susana.format_text('hell|o - w\torld', '|', '\t') as a,
   susana.format_text('hello - world', '|', '\t') as b;
   """
    goal = pd.DataFrame({"a": ["hello - world"], "b": ["hello - world"]})
    assert_frame_equal(goal, pd.read_sql(sql, susana_con))


def test_transform_pg_to_solr(susana_sample_con, solr, solr_client):
    pg_client = PostgresClient(susana_sample_con)
    file = str(resource_path_root() / "data" / "solr.csv")
    pg_client.bulk_export_solr(sql_transform, file)
    solr_core = "susana_core"
    solr_client.solr_bulk_load(solr_core, f"{SOLR_DATA}/solr.csv")
    solr = pysolr.Solr(f"{solr_client.conf}/{solr_core}", auth=solr_client.auth)
    result = solr.search("*:*")
    assert result.hits == 100


def test_transform_pg_to_solr_with_atomic_update(susana_sample_con, solr, solr_client):
    pg_client = PostgresClient(susana_sample_con)
    file = str(resource_path_root() / "data" / "solr.csv")
    pg_client.bulk_export_solr(sql_transform, file)
    solr_core = "susana_core"
    solr_client.solr_bulk_load(solr_core, f"{SOLR_DATA}/solr.csv")
    solr = SolrQuery(
        host=solr_client.conf.host,
        port=solr_client.conf.port,
        core=solr_core,
        auth=solr_client.auth,
    )
    solr.set_is_mapped("2002025431", is_standard=False)
    psolr = pysolr.Solr(f"{solr_client.conf}/{solr_core}", auth=solr_client.auth)
    result = psolr.search("is_mapped:NS AND local_map_number:1")
    assert result.hits == 1

    solr.unset_is_mapped("2002025431")
    result = psolr.search("is_mapped:EMPTY AND local_map_number:0")
    assert result.hits == 1
